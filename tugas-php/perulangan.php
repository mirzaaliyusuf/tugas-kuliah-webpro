<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="tugas.css">
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <div class="css">
        <table border="1" cellpadding="10" cellspacing="0">
            <?php 
            echo "<h1>Perulangan for </h1>";
            for( $i = 1; $i <= 3; $i++ ) : ?>
                <tr>
                    <?php for($j = 1; $j <= 5; $j++) : ?>
                    <td><?= "$i,$j"; ?></td>
                    <?php endfor; ?>    
                </tr> 
            <?php endfor; ?>
        </table>
        <?php
            echo "<h1>Perulangan Do While </h1>";
            $ulangi = 0;
            do {
                echo "<p>ini adalah perulangan ke-$ulangi</p>";
                $ulangi++;
            } while ($ulangi < 10);
        ?>
    </div>
</body>
</html>

