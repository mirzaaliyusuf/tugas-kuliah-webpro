<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="tugas.css"> 
    <title>CSS Template</title>
    <meta charset="utf-8">
<style>

</style>
</head>
    <body>
        <header>
          <h4>Tugas Modul-7</h4>
        </header>
        
    <div id="Menu">
        <ul>
            <li><a href='lingkaran.php'>lingkaran</a></li>
            <li><a href='percabangan.php'>Percabangan</a></li>
            <li><a href="perhitungan.php">Perhitungan</a></li>
            <li><a href="perulangan.php">Perulangan</a></li>
        </ul>
    </div>
        
        <section>
          
          <article>
            <h1>
            <?php
                $name = "Mirza Ali Yusuf";
                $nim = "6702213011";
                $email = "mirzaaliyusuf45@gmail.com";
                $noHP = "082164867475";
                $noWA = "6282164867475"; //Wajib pakai kode negara (kode Indonesia = 62)
                $tglLahir = "2002-12-15";
                $umur = hitungUmur($tglLahir);

                function hitungUmur($birthdayDate){
                $date = new DateTime($birthdayDate);
                $now = new DateTime();
                $interval = $now->diff($date);
                return $interval->y;
            }

                echo "Nama Saya: ";
                echo $name;
                echo "<br>";
                echo "NIM Saya: ";
                echo $nim;
                echo "<br>";
                echo "Email Saya: ";
                echo " <a href='mailto://".$email."' target='_blank'>".$email."</a>";
                echo "<br>";
                echo "No HP Saya: ";
                echo " <a href='tel://".$noHP."' target='_blank'>".$noHP."</a>";
                echo "<br>";
                echo "No WA Saya: ";
                echo " <a href='https://wa.me/".$noWA."' target='_blank'>".$noWA."</a>";
                echo "<br>";
                echo "Tgl Lahir Saya: ";
                echo $tglLahir;
                echo "<br>";
                echo "Umur Saya: ";
                echo $umur." Tahun";
                echo "<br>";
                echo "2 Tahun yang lalu saya berumur ".($umur-2)." Tahun<br>";
                echo "Tahun depan saya akan berumur ".($umur+1)." Tahun<br>";
            ?>
            </h1>
          </article>
        </section>
        
        <footer>
          <p>2022 &copy; Telkom University</p>
        </footer>
        
    </body>
</html>